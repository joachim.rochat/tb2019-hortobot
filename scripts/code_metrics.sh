echo "Total .py LOC:"
( find ../ -name '*.py' -print0 | xargs -0 cat ) | wc -l
echo ""

echo "Total .sh LOC:"
( find ../ -name '*.sh' -print0 | xargs -0 cat ) | wc -l
echo ""

echo "Inference .py LOC:"
( find ../inference -name '*.py' -print0 | xargs -0 cat ) | wc -l
echo ""