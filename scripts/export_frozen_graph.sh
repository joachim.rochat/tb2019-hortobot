OD=~/tensorflow/models/research/object_detection
CKPT=50000
python ${OD}/export_inference_graph.py \
    --input_type image_tensor \
    --pipeline_config_path gs://${BUCKET}/train/pipeline.config \
    --trained_checkpoint_prefix gs://${BUCKET}/train/model.ckpt-${CKPT} \
    --output_directory ../export-$CKPT/

