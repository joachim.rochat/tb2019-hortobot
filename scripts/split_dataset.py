# Generate a training and a validation dataset from
# a directory containing randomly names samples
import glob
import random
import shutil

# Consts
SAMPLES_DIR = 'samples'
SHUFFLE_SAMPLES = True
VAL_PROPORTION = 0.2

# Get all samples filenames
samples = glob.glob(SAMPLES_DIR + '/*.jpg')

# Shuffle the list
if SHUFFLE_SAMPLES:
	random.shuffle(samples)

# Get the number of training and validation samples
nbr_val_samples = int(VAL_PROPORTION * len(samples))
nbr_train_samples = len(samples) - nbr_val_samples

# Training samples
for m in range(nbr_train_samples):
	shutil.move(samples[m], '{}/train/train-{:04d}.jpg'.format(SAMPLES_DIR, m))

# Validation samples
for m in range(nbr_val_samples):
	shutil.move(samples[m + nbr_train_samples], '{}/val/val-{:04d}.jpg'.format(SAMPLES_DIR, m))
