# Use this script to set GCS environment variables
export PROJECT=tb2019-hortobot
export BUCKET=tb2019-hortobot-bucket
export TPU_ACCOUNT=service-233054490608@cloud-tpu.iam.gserviceaccount.com
export CONFIG_FILE=gs://${BUCKET}/data/pipeline.config
bash