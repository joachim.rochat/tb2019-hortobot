import numpy as np
import cv2 as cv
from PIL import Image, ImageDraw, ImageFont, ImageColor

## Consts
# Camera K matrix
CAMERA_K_MATRIX = np.array([
    [6.63773538e+03, 0., 3.12073138e+02],
	[0., 6.46702198e+03, 2.41493124e+02],
	[0., 0., 1.]
])

# Camera distortion ceofficients
DISTORTION_COEF = np.array([
    -1.13295395e+02, 
    1.02583866e+04, 
    -2.93292659e-03, 
    -5.12149962e-02, 
    7.98773448e+01
])

# Neural network input size (w,h)
NET_INPUT_SIZE = (300,300)

# Camera samples size (w,h)
CAM_INPUT_SIZE = (640,480)

# Unwarp alpha param
ALPHA = 0   # 1: Keep all source pixels in dest; 0: Resize

# Adjusted camera K matrix
NEW_CAMERA_K_MATRIX, ROI = cv.getOptimalNewCameraMatrix(
    CAMERA_K_MATRIX, 
    DISTORTION_COEF,
    CAM_INPUT_SIZE,
    ALPHA
)

# Pixels Look-up tables
MAPX, MAPY = cv.initUndistortRectifyMap(
    CAMERA_K_MATRIX,
    DISTORTION_COEF,
    None,
    NEW_CAMERA_K_MATRIX,
    CAM_INPUT_SIZE, 
    5
)

# FOV area size
CAM_AREA_W = 763.
CAM_AREA_H = 572.

# Camera to soil distance
CAM_TO_SOIL = 31.5

# Preprocess a camera frame
def preprocessFrame(frame):
    frame = cv.remap(frame, MAPX, MAPY, cv.INTER_LINEAR)
    return cv.resize(frame, NET_INPUT_SIZE)

# Compute the size of the plant using the size of the box in cm²
# The size is rounded to the nearest integer to 
def computeSizeFromBox(box):
    # Expand box into coordinates
    (x1,y1,x2,y2) = tuple(box)

    # Convert to the classic coordinates system
    x1p = y1
    y1p = 1 - x1
    x2p = y2
    y2p = 1- x2

    # Convert to working coordinates
    x1p *= CAM_AREA_W
    x2p *= CAM_AREA_W
    y1p *= CAM_AREA_H
    y2p *= CAM_AREA_H

    # Take into account the distance from the camera to the ground
    h = lambda d : np.sqrt([CAM_TO_SOIL**2 + d**2])[0]
    x1p = h(x1p)
    x2p = h(x2p)
    y1p = h(y1p)
    y2p = h(y2p)

    # Compute the surface of the box in cm²
    size = abs((x2p - x1p) * (y2p - y1p)) * 1E-3

    return int(round(size))

# Convert box normalized to working area coordinates
def normalizedBoxToWorkingCoords(box):
    # The box is defined by a set of two coordinates
    (x1,y1,x2,y2) = tuple(box)

    # Convert to the classic coordinates system
    x1p = y1
    y1p = 1 - x1
    x2p = y2
    y2p = 1- x2

    # The objective is assumed to be at the geometric center of the box
    x = np.mean([x1p, x2p])
    y = np.mean([y1p, y2p])

    # Normalized to working coordinates
    x *= CAM_AREA_W
    y *= CAM_AREA_H

    # Take the distance from the camera to the ground into account
    x = np.sqrt([CAM_TO_SOIL**2 + x**2])[0]
    y = np.sqrt([CAM_TO_SOIL**2 + y**2])[0]

    # Center the coordinates system origin
    x -= (CAM_AREA_W / 2)
    y -= (CAM_AREA_H / 2)

    # Return the converted coordinates
    return x,y

# Get boxes sorted by distance (closest first)
def sortBoxesByDistance(boxes, scores, classes, xlimit=0, ylimit=0):
    # Compute wokring coordinates and boxes
    working_coords = []
    distances = []
    for box in boxes:
        x,y = normalizedBoxToWorkingCoords(box)
        working_coords.append([x,y])
        distance = np.sqrt([x**2 + y**2])[0]
        distances = np.append(distances, distance)

    # Sort by closest
    sorted_indexes = np.argsort(distances)   
    working_coords = np.array(working_coords)
    working_coords = working_coords[sorted_indexes]    
    boxes = boxes[sorted_indexes]
    scores = scores[sorted_indexes]
    classes = classes[sorted_indexes]

    # Remove plants that are off-limits
    indexes_to_delete = []   
    for index in range(len(boxes)):
        # If the coordinates out of bounds,
        # remove the corresponding array entries        
        if abs(working_coords[index,0]) > xlimit or \
            abs(working_coords[index,1]) > ylimit:
            indexes_to_delete.append(index)
    
    # Delete out of bounds coordinates
    working_coords = np.delete(working_coords, indexes_to_delete, 0)
    boxes = np.delete(boxes, indexes_to_delete, 0)
    scores = np.delete(scores, indexes_to_delete)
    classes = np.delete(classes, indexes_to_delete)
        
    return working_coords, boxes, scores, classes

# Draw a status text at the bottom of the screen
def displayStatusOnImage(image_array, status, bgcolor='green', fontcolor='white'):
    # Parse input colors
    bgcolor = ImageColor.getrgb(bgcolor)
    fontcolor = ImageColor.getrgb(fontcolor)

    # Convert the array to a pillow image
    image = Image.fromarray(image_array)
    im_width, im_height = image.size

    # Load the font
    font = ImageFont.load_default()

    # Get a drawing context
    draw = ImageDraw.Draw(image)

    # Text padding
    padding = 8

    # Create a rectangle based on status size
    txt_width, txt_height = draw.textsize(status, font=font)
    x0 = 0
    y0 = im_height
    x1 = txt_width + 2 * padding
    y1 = im_height - txt_height - 2 * padding
    draw.rectangle([x0,y0,x1,y1], fill=bgcolor)

    # Draw the status
    draw.text(
        (padding, im_height - txt_height - padding),
        status,
        font=font,
        fill=fontcolor
    )

    # Replace the original image array
    new_image = np.zeros((300,300,3), dtype=np.uint8)
    np.copyto(new_image, np.array(image))

    return new_image
