import tensorflow as tf
import numpy as np
import sys
import os
import time

## Consts
# Below this score, predictions will be ignored
SCORE_THRESHOLD = 0.8
MAX_CLASSES = 2
# Home directory                                 
HOME_DIR = os.path.expanduser('~')  
# Project directory          
PROJECT_DIR = HOME_DIR + '/tb2019-hortobot'  
# Path to frozen inference graph and label map        
PATH_TO_GRAPH = PROJECT_DIR + '/export-50000/frozen_inference_graph.pb'    
PATH_TO_LABELS = PROJECT_DIR + '/dataset/label_map.pbtxt'

# Append tensorflow models for utils
sys.path.append(HOME_DIR + '/tensorflow/models')
from object_detection.utils import label_map_util, visualization_utils

# Object detection class
class ObjectDetectionPredict():
    def __init__(self):
        label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
        categories = label_map_util.convert_label_map_to_categories(
            label_map, max_num_classes=MAX_CLASSES, use_display_name=True)
        self.score_threshold = SCORE_THRESHOLD
        self.category_index = label_map_util.create_category_index(categories)
        self.load_tf_graph(PATH_TO_GRAPH)

    # Load Tensorflow graph protobuf into memory
    def load_tf_graph(self, model_path):
        print('Started loading graph')
        loading_graph_start = time.perf_counter()
        self.detection_graph = tf.Graph()
        with self.detection_graph.as_default():
            od_graph_def = tf.GraphDef()
            with tf.gfile.GFile(model_path, 'rb') as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name='')

            self.sess = tf.Session(graph=self.detection_graph)
        elapsed = time.perf_counter() - loading_graph_start
        print('Finished loading graph in {:.3f}s'.format(elapsed))
        return 0

    # Object detection
    def detect_objects(self, image_np):
        # Record prediction time
        prediction_time_start = time.perf_counter()
        
        # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(image_np, axis=0)
        image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')

        # Each box represents a part of the image where a particular object was detected.
        boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')

        # Each score represent the level of confidence for each of the objects.
        # Score is shown on the result image, together with the class label.
        scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
        classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
        num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')

        # Actual detection.
        (boxes, scores, classes, num_detections) = self.sess.run(
            [boxes, scores, classes, num_detections],
            feed_dict={image_tensor: image_np_expanded})

        # Squeeze output arrays
        boxes = np.squeeze(boxes)
        classes = np.squeeze(classes).astype(np.int32)
        scores = np.squeeze(scores)

        # Remove predictions of which the score is below the threshold
        insufficient_score_index = 0
        while insufficient_score_index < scores.size:
            if scores[insufficient_score_index] < self.score_threshold:
                break
            insufficient_score_index = insufficient_score_index + 1
        boxes = boxes[:insufficient_score_index]
        classes = classes[:insufficient_score_index]
        scores = scores[:insufficient_score_index]

        # Visualization of the results of a detection.
        visualization_utils.visualize_boxes_and_labels_on_image_array(
            image_np,
            boxes,
            classes,
            scores,
            self.category_index,
            use_normalized_coordinates=True,
            line_thickness=2)
            
        # Display prediction time
        elapsed = time.perf_counter() - prediction_time_start
        print('Prediction time = {:.3f}s'.format(elapsed))
        
        return boxes, scores, classes, image_np

    def close(self):
        self.sess.close()
