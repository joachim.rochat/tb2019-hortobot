## Consts
# Location margin for duplicates (in cm)
DUPLICATE_MARGIN = 2

# Plant class
class Plant:
    # Plant class constructor
    def __init__(self, pclass=1, x=0.0, y=0.0, size=0):
        # Prediction class
        self.pclass = pclass

        # Plant location
        self.x = x
        self.y = y

        # Plant size
        self.size = size

# Plant database class
class PlantDb:
    # Plant database constructor
    def __init__(self):
        # Plants database
        self.database = []

    # If the input plant is not a duplicate
    # add it to the database
    def checkDuplicateAndAdd(self, plant):
        # Duplicate flag
        duplicate = False

        # Growth variable
        growth = 0.0

        # Check if any plant is close enough to be considered a duplicate
        for index, entry in enumerate(self.database):
            if abs(entry.x - plant.x) <= DUPLICATE_MARGIN and \
                abs(entry.y - plant.y) <= DUPLICATE_MARGIN and \
                entry.pclass == plant.pclass:
                # Print status
                print('INFO: Duplicate found, entry updated')

                # Display plant growth
                growth = -(1 - (plant.size / entry.size))
                print('INFO: Plant growth = {:.1%}'.format(growth))
                print('INFO: Previous size = {:d}cm^2; New size = {:d}cm^2'.format(
                    plant.size,
                    entry.size
                ))

                # Update the size
                duplicate = True
                self.database[index].size = plant.size
                break
        
        # If no duplicate was found, insert the new plant in the database
        if not duplicate:
            # Insert the new entry
            self.database.append(plant)

            # Print status
            print('INFO: No duplicate found, new entry inserted')

        return growth
