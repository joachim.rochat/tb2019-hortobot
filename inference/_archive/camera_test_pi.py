import time
import cv2 as cv
import numpy as np
from platform import uname
import time

class Camera:
	def __init__(self):
		if uname()[1] == 'raspberrypi':
			print('Detected platform: RPi')
			self.platform = 'RPi'
			from picamera.array import PiRGBArray
			from picamera import PiCamera
			self.stream = PiCamera()
			self.stream.rotation = 180
			self.stream.resolution = (640, 480)
			self.stream.framerate = 32

		else:
			print('Detected platform: Other')
			import cv2 as cv
			self.platform = 'Other'
			self.stream = cv.VideoCapture(0)

		# Camera warmup
		time.sleep(1)

	def getFrame(self):
		if self.platform == 'RPi':
			output = np.empty((480, 640, 3), dtype=np.uint8)
			self.stream.capture(output, format='bgr', use_video_port=True)
			return output
		else:
			_, frame = self.stream.read()
			return frame

	def close(self):
		if self.platform == 'Other':
			self.stream.release()

vs = Camera()

# Define camera matrix K
K = np.array([[6.63773538e+03, 0., 3.12073138e+02],
			  [0., 6.46702198e+03, 2.41493124e+02],
			  [0., 0., 1.]])

# Define distortion coefficients d
d = np.array([-1.13295395e+02, 1.02583866e+04, -2.93292659e-03, -5.12149962e-02, 7.98773448e+01])

# Generate new camera matrix from parameters
newcameramatrix, roi = cv.getOptimalNewCameraMatrix(K, d, (640,480), 0)

# Generate look-up tables for remapping the camera image
mapx, mapy = cv.initUndistortRectifyMap(K, d, None, newcameramatrix, (640, 480), 5)

# capture frames from the camera
while True:
	# grab the raw NumPy array representing the image, then initialize the timestamp
	# and occupied/unoccupied text
	image = vs.getFrame()
	image = cv.remap(image, mapx, mapy, cv.INTER_LINEAR)

	image[240,320] = [0x00,0xff,0x00]
	image[360,320] = [0x00,0xff,0x00]

	# show the frame
	cv.imshow("Frame", image)
	key = cv.waitKey(1) & 0xFF

	# if the `q` key was pressed, break from the loop
	if key == ord("q"):
		break
