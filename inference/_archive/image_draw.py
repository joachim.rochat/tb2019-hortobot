import numpy as np
from PIL import Image, ImageDraw, ImageFont, ImageColor

status = "Hello world !"

# Make a blank image array
image_array = np.zeros((300,300,3), dtype=np.uint8)

bgcolor = 'OrangeRed'
fontcolor = 'white'

## Code to add: start

# Parse input colors
bgcolor = ImageColor.getrgb(bgcolor)
fontcolor = ImageColor.getrgb(fontcolor)

# Convert the array to a pillow image
image = Image.fromarray(image_array)
im_width, im_height = image.size

# Load the font
font = ImageFont.load_default()

# Get a drawing context
draw = ImageDraw.Draw(image)

# Text padding
padding = 8

# Create a rectangle based on status size
txt_width, txt_height = draw.textsize(status, font=font)
x0 = 0
y0 = im_height
x1 = txt_width + 2 * padding
y1 = im_height - txt_height - 2 * padding
draw.rectangle([x0,y0,x1,y1], fill=bgcolor)

# Draw the status
draw.text(
    (padding, im_height - txt_height - padding),
    status,
    font=font,
    fill=fontcolor
)

# Replace the original image array
np.copyto(image_array, np.array(image))


## Code to add: end

image.show()
