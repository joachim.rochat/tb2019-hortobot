import cv2

# Start default camera
video = cv2.VideoCapture(0)

while video.isOpened():
    ret, frame = video.read()
    # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2YUYV)
    cv2.imshow("frame", frame)

    key = cv2.waitKey(1) & 0xFF
    # if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break
# Release video
video.release()
cv2.destroyAllWindows()