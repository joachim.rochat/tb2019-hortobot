import psutil
from gpiozero import CPUTemperature

while True:
    print("CPU usage: {:.2%}".format(psutil.cpu_percent() / 100))
    print("Memory usage: {:.2%}".format(psutil.virtual_memory().percent / 100))
    try:
        print("CPU temp: {}°C".format(CPUTemperature().temperature))
    except:
        print('Could not fetch CPU temp')
    input("Get info")