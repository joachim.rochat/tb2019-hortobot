from platform import uname
import numpy as np
import time

# Camera class
class Camera:
    # Camera class constructor
    def __init__(self):
        # For RPi
        if uname()[1] == 'raspberrypi':
            self.platform = 'RPi'
            from picamera.array import PiRGBArray
            from picamera import PiCamera
            self.stream = PiCamera()
            self.stream.rotation = 180
            self.stream.resolution = (640, 480)
            self.stream.framerate = 32

        # For other platforms
        else:
            import cv2 as cv
            self.platform = 'Other'
            self.stream = cv.VideoCapture(0)

    # Get a camera frame 
    def getFrame(self):
        # For RPi
        if self.platform == 'RPi':
            output = np.empty((480, 640, 3), dtype=np.uint8)
            self.stream.capture(output, format='bgr', use_video_port=False)
            return output

        # For other platforms
        else:
            _, frame = self.stream.read()
            return frame

    def close(self):
        # For other platforms
        if self.platform == 'Other':
            self.stream.release()
