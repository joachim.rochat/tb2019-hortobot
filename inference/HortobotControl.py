import cv2 as cv
import os
import time
import numpy as np

import ObjectDetectionPredict
import Camera
import PlatformInterface
import HortobotUtils
import WorkingAreaMap
import PlantDb
import Diagnostics

## Consts
# Frame display size
FRAME_DISP_SIZE = (1000,1000)

# Minimal system precision of 10mm
LOC_PRECISION = 10

# Minimal estimated location precision
EST_PRECISION = 50

# Standard colors
COLOR_ORANGE = '#e56419'
COLOR_RED = '#d60000'
COLOR_GREEN = '#2db700'
COLOR_BLUE = '#002fbf'

# Hortobot control class
class HortobotControl:
    def __init__(self):
        # Initialize video stream
        self.camera = Camera.Camera()

        # Object detection class instance
        self.object_detection = ObjectDetectionPredict.ObjectDetectionPredict()

        # Platform interface instance
        self.platform_interface = PlatformInterface.PlatformInterface()

        # Working area map instance
        self.working_area_map = WorkingAreaMap.WorkingAreaMap(
            (PlatformInterface.AREA_X, PlatformInterface.AREA_Y),
            (HortobotUtils.CAM_AREA_W, HortobotUtils.CAM_AREA_H)
        )

        # Plant database, gets populated on runtime
        self.plant_db = PlantDb.PlantDb()

        # Current status display text
        self.status = 'No detection'
        self.status_bgcolor = COLOR_GREEN
        self.status_fontcolor = 'white'
    
        # Camera frame
        self.frame = np.zeros((300,300,3), dtype=np.uint8)

        # New dir for frames
        ts = int(time.time())
        self.frames_dir = 'frames-{}'.format(ts)
        os.mkdir(self.frames_dir)

    # Generate object samples
    def generateSamples(self):
        # Position delta between waypoints in mm
        delta = 75

        # Position change lambdas
        up = lambda: [0,delta]
        down = lambda: [0,-delta]
        right = lambda: [delta,0]
        left = lambda: [-delta,0]

        # Relative waypoints
        rel_waypoints = [
            [0,0], down(), left(),
            up(), up(),
            right(), right(),
            down(), down(), down(),
            left(), left(), left(),
            up(), up(), up(), up(),
            right(), right(), right(), right(),
            down(), down(), down(), down()
        ]

        # Object location in the working area
        obj_location_x = 480
        obj_location_y = 450

        # Move to the object location
        self.platform_interface.moveToCoord(
            x = obj_location_x,
            y = obj_location_y
        )

        # Wait for operator action to continue
        input("Press Enter to continue...")

        # Get a timestamp
        ts = int(time.time())

        # Create a dir name
        dir = 'samples-{}'.format(ts)

        # Create a folder to store the collected samples
        os.mkdir(dir)

        # Make relative moves and take samples
        for index, rel_waypoint in enumerate(rel_waypoints):
            # Expand the waypoint
            (xOffset,yOffset) = tuple(rel_waypoint)

            # Move the robot
            self.platform_interface.relativeMove(
                xOffset,
                yOffset
            )

            # Pause to prevent camera shaking
            time.sleep(50E-3)

            # Get a video frame and preprocess it
            frame = self.camera.getFrame()
            frame = HortobotUtils.preprocessFrame(frame)

            # Save it
            cv.imwrite('{}/sample-{:04d}.jpg'.format(dir, index), frame)

    # Get a frame, run inference and display results
    def inference(self):
        # Get a video frame and preprocess it
        frame = self.camera.getFrame()
        frame = HortobotUtils.preprocessFrame(frame)

        # Run inference
        boxes, scores, classes, self.frame = \
            self.object_detection.detect_objects(frame)

        # Display system diagnostics
        Diagnostics.log_info()

        # Show the detection results
        self.__displayFrame()

        # Get working coordinates and filter plants that belong
        # to the next region
        if len(boxes) > 0:
            # Sort the boxes, scores and classes by plant proximity
            # and get their working coodinates
            coords, boxes, scores, classes = HortobotUtils.sortBoxesByDistance(
                boxes,
                scores,
                classes,
                xlimit=(WorkingAreaMap.GRID_WIDTH / 2) + 30,
                ylimit=(WorkingAreaMap.GRID_HEIGHT / 2) + 30
            )

        # Else, assign empty arrays
        else:
            coords, boxes, scores, classes = [],[],[],[]

        return coords, boxes, scores, classes

    # Returns true if the plant is considered to be aligned
    def is_aligned(self, coords):
        # Aligned flag
        aligned = False

        # Out of bounds flag
        out_of_bounds = False

        # Expand coordinates
        (x,y) = tuple(coords)

        # Check estimate precision
        if abs(x) > EST_PRECISION or abs(y) > EST_PRECISION:
            out_of_bounds = True

        # Check location precision
        if abs(x) <= LOC_PRECISION and abs(y) <= LOC_PRECISION:
            aligned = True

        return out_of_bounds, aligned

    # Adjust position as long as it is not aligned
    def align_plant(self, coords):
        # Expand plant coordinates
        (x,y) = tuple(coords)
        
        # Print status
        print('INFO: Started alignment procedure at ({:.3f},{:.3f})'.format(x,y))
        self.__printStatus(
            'Started alignment procedure',
            bgcolor=COLOR_ORANGE
        )

        # Plant is aligned flag
        aligned = False
        
        # Alignment moves counter
        alignment_moves = 1

        # Alignment procedure timing
        alignment_start = time.perf_counter()

        # As long as the plant is not aligned
        while not aligned:
            # Move to the predicted coordinates
            self.platform_interface.relativeMove(x,y)
            time.sleep(25E-3)

            # Increment the alignment move counter
            alignment_moves += 1

            # Inference
            coords, boxes, _, classes = self.inference()

            # Get the closest plant
            if len(coords) > 0:
                # Check alignment
                (x,y) = tuple(coords[0])
                out_of_bounds, aligned = self.is_aligned((x,y))

                # If the plant is out of bounds
                if out_of_bounds:
                    print('Position = ({:.3f},{:.3f})'.format(x,y))
                    print("WARNING: The plant was lost during alignment (OUT OF BOUNDS)")
                    self.__printStatus(
                        'The plant was lost',
                        COLOR_RED
                    )
                    break

                # If it is aligned, print status
                elif aligned:
                    # Print status
                    elapsed = time.perf_counter() - alignment_start
                    print(
                        'INFO: The alignment was done in {:.3f}s with {} moves'.format(
                            elapsed, 
                            alignment_moves
                        )
                    )
                    
                    # Compute the size of the plant
                    size = HortobotUtils.computeSizeFromBox(boxes[0])

                    # Add the plant to the database
                    plant = PlantDb.Plant(
                        pclass=classes[0],
                        x=x,
                        y=y,
                        size=size
                    )
                    growth = self.plant_db.checkDuplicateAndAdd(plant)
                    self.__printStatus(
                        'Size = {:.3f}cm² | Growth = {:.1%}'.format(
                            size,
                            growth
                        ),
                        bgcolor=COLOR_BLUE
                    )
                    time.sleep(5)

            # No plant was detected, the tracked plant was lost
            else:
                print("WARNING: The plant was lost during alignment (NO DETECTION)")
                self.__printStatus(
                    'The plant was lost',
                    bgcolor=COLOR_RED
                )
                break

    # Run the control procedure of the platform
    def run(self, nbr_iter=1):
        # Run a warmup prediction
        _,_,_,_ = self.inference()
        
        # Sweep the working area the given number of times
        for m in range(nbr_iter):
            # Print status
            print('\nINFO: Working area sweep no. {:d}'.format(m + 1))

            # Reset waypoints
            self.working_area_map.reset()

            # Area sweep
            self.__sweepWorkingArea()
    
    # Print current status
    def __printStatus(self, status, fontcolor='white', bgcolor='green'):
        self.status = status
        self.status_bgcolor = bgcolor
        self.status_fontcolor = fontcolor
        print(status)
        self.__displayFrame()

    # Display a frame (numpy array) on screen
    def __displayFrame(self):
        # Annotate the last buffered frame with the status
        new_frame = HortobotUtils.displayStatusOnImage(
            self.frame, 
            self.status, 
            self.status_bgcolor,
            self.status_fontcolor
        )

        # Save the frame on the disk
        ts = int(time.time())
        img_path = os.path.join(self.frames_dir, 'frame-{}.jpg'.format(ts))
        cv.imwrite(img_path, new_frame)

        # Display the new frame
        cv.imshow("Plant detection", cv.resize(new_frame, FRAME_DISP_SIZE))
        _ = cv.waitKey(1) & 0xFF
        time.sleep(50E-3)

    # Sweep the area and look for plants
    def __sweepWorkingArea(self):
        # Execution loop
        while True:
            # Get the next waypoint
            last_waypoint, (x,y) = self.working_area_map.getNextWayPoint()

            # Move to the waypoint
            self.platform_interface.moveToCoord(x,y)

            # Status
            self.__printStatus(
                'Moving to next waypoint',
                bgcolor=COLOR_GREEN
            )

            # Inference
            coords, _, _, _ = self.inference()

            # Go over each plant, starting with the closest
            for coord in coords:
                # Expand coordinates
                (x,y) = tuple(coord)

                # Adjust position as long as it is not aligned
                self.align_plant((x,y))

            # If this is the last waypoint break out of the loop
            if last_waypoint:
                break

    # This method should be called to recycle this object
    def close(self):
        # Instances cleanup
        self.object_detection.close()
        cv.destroyAllWindows()
        self.camera.close()

    # Class destructor
    def __del__(self):
        self.close()
