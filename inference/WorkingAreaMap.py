import numpy as np

## Consts
# Grid size
GRID_WIDTH = 320.
GRID_HEIGHT = 285.

# Waypoints
WAYPOINTS = [
    [1*GRID_WIDTH/2, 1*GRID_HEIGHT/2],
    [3*GRID_WIDTH/2, 1*GRID_HEIGHT/2],
    [5*GRID_WIDTH/2, 1*GRID_HEIGHT/2],

    [5*GRID_WIDTH/2, 3*GRID_HEIGHT/2],
    [3*GRID_WIDTH/2, 3*GRID_HEIGHT/2],
    [1*GRID_WIDTH/2, 3*GRID_HEIGHT/2],
    
    [1*GRID_WIDTH/2, 5*GRID_HEIGHT/2],
    [3*GRID_WIDTH/2, 5*GRID_HEIGHT/2],
    [5*GRID_WIDTH/2, 5*GRID_HEIGHT/2]
]

# Working area map class
class WorkingAreaMap:
    # Working area map constructor
    def __init__(self, area_xy, fov_xy):
        # Waypoints
        self.waypoints = WAYPOINTS

        # Current waypoint index
        self.currentWaypointIndex = 0

    # Reset the map for the next sweep
    def reset(self):
        self.currentWaypointIndex = 0

    # Get the next waypoint coordinates
    def getNextWayPoint(self):
        # Last waypoint flag
        last_waypoint = False

        # If this is the last waypoint
        if len(self.waypoints) == (self.currentWaypointIndex + 1):
            last_waypoint = True

        # Get the next waypoint
        waypoint = self.waypoints[self.currentWaypointIndex]

        # Increment waypoint index
        self.currentWaypointIndex += 1

        # Expand the waypoint coordinates
        (x,y) = tuple(waypoint)

        # Return the flag and the waypoint
        return last_waypoint, (x,y)
