import serial
import serial.tools.list_ports as list_ports
import time

## Consts
# Axes area boundaries
AXES_LIMITS = {
	'X_MIN': 0,
	'X_MAX': 960,
	'Y_MIN': 0,
	'Y_MAX': 900
}
AREA_X = AXES_LIMITS['X_MAX'] - AXES_LIMITS['X_MIN']
AREA_Y = AXES_LIMITS['Y_MAX'] - AXES_LIMITS['Y_MIN']

# When set to true, __sendCommand() will return when the 
# platform goes back to idle, it is used as a way to make
# sending commands a blocking operation
BLOCK_UNTIL_IDLE = True

# Platform states
STATE_IDLE = 'Idle'
STATE_RUN = 'Run'

# Echo serial input and output (to debug serial)
ECHO_SERIAL_LINES = False

# This class is used to communicate with the platform by using functions
# instead of hardcoded commands
class PlatformInterface:
	# Constructor of the platform interface
	def __init__(self):
		# Serial com instance
		self.com = self.__getNewCom()

		# Command counter
		self.commandCounter = 0

		# Axes limits
		self.axes_limits = AXES_LIMITS

		# Verify platform startup sequence
		self.__checkStartupSequence()

		# Refresh current status
		self.current_state = None
		self.current_x = None
		self.current_y = None
		self.__refreshStatus()

	# Verify platform startup sequence by querying and checking specific values
	def __checkStartupSequence(self):
		startup_sequence = [
			self.__readLine(),
			self.__readLine(),
			self.__readLine()
		]
		if startup_sequence[0] != '' or \
			startup_sequence[1] != "Grbl 0.8 ['$' for help]" or \
			startup_sequence[2] != 'F300S1000{0/0}ok':
			raise Exception('Invalid platform startup sequence')

	# Get a serial com instance
	def __getNewCom(self):
		# List available ports
		devices = list_ports.comports()

		# Find the platform UART interface
		platform = None
		for device in devices:
			if 'CH340' in device.description or device.description == 'USB2.0-Serial':
				platform = device.device
				break

		# If the device was not found
		if platform == None:
			raise Exception('The platform is not connected to your computer.')
		else:
			print('The platform was found in the connected devices list')
			print('Establishing communication to the platform')

		# Connect to the platform
		com = serial.Serial(
			port=platform,					# Connect to platform port
			baudrate=115200,				# 115'200 Baud/s
			bytesize=serial.EIGHTBITS,		# 8 bits data
			parity=serial.PARITY_NONE,		# No parity bit
			stopbits=serial.STOPBITS_ONE,	# One stop bit
			timeout=1.0,					# .5s handshake timeout
			xonxoff=0,						# No flow control
			rtscts=0,
			dsrdtr=0,
			write_timeout=.5,				# .5s write timeout
			inter_byte_timeout=None 		# No inter byte timeout
			)
		print('Successfully connected to the platform on port {}'.format(platform))
		
		# HACK: Hey, this won't work without a timeout !
		time.sleep(500E-3)

		# Return the com session
		return com

	# Send a command over the UART line to the platform
	def __sendCommand(self, cmd_name, cmd):
		# Send the command
		self.__writeLine(cmd)

		# Read the answer from the platform
		read_line = self.__readLine()

		# Log the command and the answer
		if 'ok' in read_line:
			padding_cmd = " " * (16 - len(cmd_name))
			padding_ack = " " * (12 - len(cmd))
			print('[{0}] Command sent: {1}{2}({3}){4}(OK)\n'.format(self.commandCounter, \
				cmd_name, padding_cmd, cmd, padding_ack), flush=True)
		else:
			raise Exception('The command "{}" was not acknowledged by the platform', cmd)

		# Wait for IDLE state
		if BLOCK_UNTIL_IDLE:
			# TODO: Implement timeout on this loop (using a counter ?)
			while True:
				# Refresh platform status
				self.__refreshStatus()

				# Break out of the loop if we are in IDLE mode
				if self.current_state == STATE_IDLE:
					break

				# Else wait a bit before asking again
				time.sleep(50E-3)

		# Update command counter
		self.commandCounter = self.commandCounter + 1

	# Retreive relevant information from the status
	def __refreshStatus(self):
		# Request platform status
		self.__writeLine('?', newLine=False)

		# Extract status info
		# Typical status: 
		# <Idle,MPos:200.000,200.000,0.000,WPos:200.000,200.000,0.000,RX:1,1/1>
		status = self.__readLine()

		# Strip start/end chars
		status = status.replace('<', '')
		status = status.replace('>', '')

		# Split the status
		split_status = status.split(',')

		# Get relevant parts
		self.current_state = split_status[0]
		x = (split_status[4]).split(':')
		self.current_x = round(float(x[1]))
		self.current_y = round(float(split_status[5]))

	# Read and decode from serial line
	def __readLine(self):
		read_line = self.com.read_until().decode('ascii')
		if ECHO_SERIAL_LINES:
			print("Read: {}".format(read_line), flush=True)
		return read_line.replace('\r\n', '')

	# Write to serial com and flush buffers (When newLine is set to True (default), insert a newline after the command)
	def __writeLine(self, cmd, newLine=True):
		if ECHO_SERIAL_LINES:
			print('Written: {}'.format(cmd), flush=True)
		if newLine:
			cmd = cmd + '\n'
		self.com.write(cmd.encode('ascii'))
		self.com.flush()

	# Check if given coordinates are part of the working area
	def __checkCoordinates(self, x=None, y=None):
		if x == None and y == None:
			raise Exception('Invalid coordinates given: ({},{})'.format(x,y))
		else:
			if x == None:
				x = self.axes_limits['X_MIN']
			if y == None:
				y = self.axes_limits['Y_MIN']
			if not (self.axes_limits['X_MIN'] <= x <= self.axes_limits['X_MAX']) \
				or not (self.axes_limits['Y_MIN'] <= y <= self.axes_limits['Y_MAX']):
				raise Exception('Out of bound coordinates given: ({},{})'.format(x,y))

	# Set the current location as working coordinates zero
	def setZero(self):
		self.__sendCommand('SET_ZERO', 'G92X0Y0')

	# Move relatively to current position
	def relativeMove(self, xOffset=0, yOffset=0):
		x = self.current_x + xOffset
		y = self.current_y + yOffset
		self.moveToCoord(x,y)

	# Move to specified coordinates
	def moveToCoord(self, x=None, y=None):
		# Verify coordinates
		self.__checkCoordinates(x, y)
		
		# Build the command
		cmd = ''
		if x != None:
			cmd = cmd + 'X{}'.format(x)
		if y != None:
			cmd = cmd + 'Y{}'.format(y)
		self.__sendCommand('MOVE_TO_COORD', cmd)

	# Move to (0,0) working coordinates as fast as possible
	def home(self):
		self.__sendCommand('HOME', 'G0X0Y0')

	# Configure maximum speed
	def setMaxSpeed(speed):
		self.__sendCommand('SET_MAX_SPEED_X', '$110={}'.format(speed))
		self.__sendCommand('SET_MAX_SPEED_Y', '$111={}'.format(speed))

	# Configure acceleration
	def setAccel(accel):
		self.__sendCommand('SET_ACCEL_X', '$120={}'.format(accel))
		self.__sendCommand('SET_ACCEL_Y', '$121={}'.format(accel))

	# Destructor, closes the serial com if it is still open
	def __del__(self):
		if self.com.is_open:
			self.home()
			self.com.close()
