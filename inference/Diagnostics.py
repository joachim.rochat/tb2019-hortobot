import psutil
from gpiozero import CPUTemperature

# Log system diagnostic info
def log_info():
    # Get CPU usage and temp, RAM usage
    cpu_usage = psutil.cpu_percent() / 100
    ram_usage = psutil.virtual_memory().percent / 100
    try:
        cpu_temp = CPUTemperature().temperature
    except:
        cpu_temp = -1.0
    
    # Print all the retreived info
    print(
        'DIAG: CPU usage = {:.1%}, CPU temp = {:.2f}°C, RAM usage = {:.1%}'.format(
            cpu_usage,
            cpu_temp,
            ram_usage
        )
    )