import HortobotControl
import sys

def main():
    # Print a program id
    program_id = "Hortobot platform control, 2019, Joachim Rochat"
    border = "#" * (len(program_id) + 8)
    print(border)
    print("##  " + program_id + "  ##")
    print(border)

    # Python version check
    if sys.version_info[0] < 3:
        raise Exception("Error: You must use Python 3")

    # Hortobot control instance
    hortobot_control = HortobotControl.HortobotControl()

    # Hortobot control run
    hortobot_control.run(nbr_iter=2)
    #hortobot_control.generateSamples()

    # Close control instance
    hortobot_control.close()

if __name__ == '__main__':
    main()
