import numpy as np
import cv2
from matplotlib import pyplot as plt

# Define camera matrix K
K = np.array([[6.63773538e+03, 0., 3.12073138e+02],
			  [0., 6.46702198e+03, 2.41493124e+02],
			  [0., 0., 1.]])

# Define distortion coefficients d
d = np.array([-1.13295395e+02, 1.02583866e+04, -2.93292659e-03, -5.12149962e-02, 7.98773448e+01])

# Read an example image and acquire its size
img = cv2.imread('./chessboard.jpg')
h, w = img.shape[:2]

# Generate new camera matrix from parameters
newcameramatrix, roi = cv2.getOptimalNewCameraMatrix(K, d, (w,h), 0)

# Generate look-up tables for remapping the camera image
mapx, mapy = cv2.initUndistortRectifyMap(K, d, None, newcameramatrix, (w, h), 5)

# Remap the original image to a new image
newimg = cv2.remap(img, mapx, mapy, cv2.INTER_LINEAR)

# Display old and new image
fig, (oldimg_ax, newimg_ax) = plt.subplots(1, 2)
oldimg_ax.imshow(img)
oldimg_ax.set_title('Original image')
newimg_ax.imshow(newimg)
newimg_ax.set_title('Unwarped image')
plt.show()
