import tensorflow as tf

for example in tf.python_io.tf_record_iterator("coco_val.record"):
    result = tf.train.Example.FromString(example)
    #print(result)
    print("\n====")
    print(result.features.feature['image/filename'].bytes_list.value)  
    print(result.features.feature['image/object/class/label'].int64_list.value)
    print(result.features.feature['image/object/bbox/xmax'].float_list.value)
    print(result.features.feature['image/object/bbox/xmin'].float_list.value)
    print(result.features.feature['image/object/bbox/ymax'].float_list.value)
    print(result.features.feature['image/object/bbox/ymin'].float_list.value)
    print("====")