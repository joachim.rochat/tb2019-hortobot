'''
Assumed folder structure:
+dataset
    +train
        [.xml and .jpeg files with the same name for each sample]
    +val
        [.xml and .jpeg files with the same name for each sample]
+dataset
    -label_map.pbtxt

The dataset must be based on the PASCAL VOC format
'''

import tensorflow as tf
import sys
import os
import glob
import hashlib
import io
from lxml import etree

# Home directory                                 
HOME_DIR = os.path.expanduser('~')

# Project directory
PROJECT_DIR = os.path.join(HOME_DIR, 'tb2019-hortobot')

# Append tensorflow models for utils
sys.path.append(HOME_DIR + '/tensorflow/models')
from object_detection.utils import dataset_util, label_map_util

# Current directory
CURRENT_DIR = os.path.join(PROJECT_DIR, 'dataset') 

# Samples and annotations dir
TRAIN_DIR = os.path.join(CURRENT_DIR, 'train')
VAL_DIR = os.path.join(CURRENT_DIR, 'val')

# Label map path
LABEL_MAP_PATH = os.path.join(CURRENT_DIR, 'label_map.pbtxt')

# Output records path
TRAIN_OUTPUT = 'coco_train.record'
VAL_OUTPUT = 'coco_val.record'

# Create a Tfrecord from a PASCAL VOC dataset
def createRecordFromVoc(dataset_dir, label_map_path, output_path):
    # I/O writer
    writer = tf.python_io.TFRecordWriter(output_path)

    # Label map dictionary
    label_map_dict = label_map_util.get_label_map_dict(label_map_path)

    # Get sample identifiers
    samples = glob.glob(dataset_dir + '/*.xml')
    for index, sample in enumerate(samples):
        samples[index] = sample.split('.')[0]

    # For each sample
    for sample in samples:
        # Read the xml annotation tree
        xml_path = os.path.join(CURRENT_DIR, sample + '.xml')
        with tf.gfile.GFile(xml_path, 'r') as fid:
            xml_str = fid.read()
        xml = etree.fromstring(xml_str)
        data = dataset_util.recursive_parse_xml_to_dict(xml)['annotation']

        # Decode the image
        image_path = os.path.join(CURRENT_DIR, sample + '.jpeg')
        with tf.gfile.GFile(image_path, 'rb') as fid:
            encoded_jpg = fid.read()

        # Compute image hash
        key = hashlib.sha256(encoded_jpg).hexdigest()
        
        # Get image size
        width = int(data['size']['width'])
        height = int(data['size']['height'])

        # Populate example data
        xmin = []
        ymin = []
        xmax = []
        ymax = []
        classes = []
        classes_text = []
        truncated = []
        poses = []
        difficult_obj = []

        if 'object' in data:
            for obj in data['object']:
                if 'polygon' in obj:
                    xmin.append(float(obj['polygon']['x1']) / width)
                    ymin.append(float(obj['polygon']['y1']) / height)
                    xmax.append(float(obj['polygon']['x2']) / width)
                    ymax.append(float(obj['polygon']['y2']) / height)
                elif 'bndbox' in obj:
                    xmin.append(float(obj['bndbox']['xmin']) / width)
                    ymin.append(float(obj['bndbox']['ymin']) / height)
                    xmax.append(float(obj['bndbox']['xmax']) / width)
                    ymax.append(float(obj['bndbox']['ymax']) / height)
                else:
                    raise Exception('No annotation found on sample {}'.format(
                        sample + '.xml'
                    ))
                classes_text.append(obj['name'].encode('utf8'))
                classes.append(label_map_dict[obj['name']])
                truncated.append(int(obj['truncated']))
                poses.append(obj['pose'].encode('utf8'))

        # Create an example based on retreived sample data
        example = tf.train.Example(features=tf.train.Features(feature={
            'image/height': dataset_util.int64_feature(height),
            'image/width': dataset_util.int64_feature(width),
            'image/filename': dataset_util.bytes_feature(
                data['filename'].encode('utf8')),
            'image/source_id': dataset_util.bytes_feature(
                data['filename'].encode('utf8')),
            'image/key/sha256': dataset_util.bytes_feature(key.encode('utf8')),
            'image/encoded': dataset_util.bytes_feature(encoded_jpg),
            'image/format': dataset_util.bytes_feature('jpeg'.encode('utf8')),
            'image/object/bbox/xmin': dataset_util.float_list_feature(xmin),
            'image/object/bbox/xmax': dataset_util.float_list_feature(xmax),
            'image/object/bbox/ymin': dataset_util.float_list_feature(ymin),
            'image/object/bbox/ymax': dataset_util.float_list_feature(ymax),
            'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
            'image/object/class/label': dataset_util.int64_list_feature(classes),
            'image/object/difficult': dataset_util.int64_list_feature(difficult_obj),
            'image/object/truncated': dataset_util.int64_list_feature(truncated),
            'image/object/view': dataset_util.bytes_list_feature(poses),
        }))

        # Write the serialized record
        writer.write(example.SerializeToString())

    # Close the record writer
    writer.close()

# Main code
def main():
    print('Converting validation (VOC -> TFrecord)')
    createRecordFromVoc(VAL_DIR, LABEL_MAP_PATH, VAL_OUTPUT)
    print('Converting training (VOC -> TFrecord)')
    createRecordFromVoc(TRAIN_DIR, LABEL_MAP_PATH, TRAIN_OUTPUT) 
    print('Done')

if __name__ == "__main__":
    main()